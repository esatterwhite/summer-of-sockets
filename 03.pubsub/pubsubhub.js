// pubsubhub.js
var zmq = require('zmq')
  , xpub = zmq.socket('xpub')
  , xsub = zmq.socket('xsub')

  // pub sends to sub
xsub.on('message', xpub.send.bind(xpub));
xpub.on('message', xsub.send.bind( xsub));
// sit on some endpoints
xsub.bind( 'tcp://0.0.0.0:5202', function(){
	console.log( 'xsub', arguments )
});
xpub.bind( 'tcp://0.0.0.0:5201', function(){   
	console.log( 'xpub', arguments )
});

