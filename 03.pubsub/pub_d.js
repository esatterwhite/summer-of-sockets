// pub_b.js
var zmq = require('zmq')
  , socket = zmq.socket( 'pub' )
  , endpoint = 'tcp://localhost:5202'
  , count = 0;

socket.connect(endpoint)
 setInterval( function(){
	 console.log( 'sending %s', ++count )
	 socket.send('hello world:pub_c.js:'+process.pid + ':' + (count))
 }, 300 )
