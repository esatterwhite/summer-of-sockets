// pub_b.js
var zmq = require('zmq')
  , socket = zmq.socket( 'pub' )
  , endpoint = 'tcp://0.0.0.0:5102'
  , count = 0;

socket.bind(endpoint, function( err ){
	setInterval( function(){
		console.log( 'sending %s', ++count )
		socket.send('goodbye world:pub_b.js:'+process.pid + ':' + (count))
	}, 300 )
})
