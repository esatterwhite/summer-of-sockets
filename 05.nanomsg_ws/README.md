run server.js, open as many browser tabs / windows as you like to `localhost:3000`
Post a plain text message to `localhost:3000` 

```
$ curl -XPOST -H "Content-Type: text/plain" -d 'This is a message' http://localhost:3000
```
