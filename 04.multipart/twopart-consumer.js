// twopart-consumer.js 
var zmq = require( 'zmq' ) 
  , socket = zmq.socket( 'pull' )
  , counter = 0
  , methods
  ;

methods = {
	add:function( value ){
		value = parseInt( value, 10 )
		if( !isNaN( value ) ){
			counter += value;
			return console.log( "counter is %d", counter )
		}

		var e = new Error();
		e.name = "InvalidValueType"
		e.message = "add only accepts Numbers";
		throw e;
	}
}

// Each message part will come across as parameter 
socket.on('message', function( fn, value ){ 
    // messages come as buffers 
    var op = methods[ fn.toString('ascii') ];

    op && op( value.toString('ascii'))
});
// connect to original address 
socket.connect('tcp://0.0.0.0:9998');
