// multipart-request.js
var zmq = require( 'zmq' ) 
  , socket = zmq.socket( 'req' ) 

// bind to an address and port 
socket.bind('tcp://0.0.0.0:9998');
// send a message to forwarder
setInterval(function(){
    socket.send([
    	"GET"
    	, 1
    	,"/data"
    	,"json"
	]);
},150); 

socket.on('message', function( status, type, payload ){
	status = parseInt( status.toString() )
	if( status !== 200 ){
		// retry request
		console.error("bad request")
	} else {
		console.log( payload )
	}
	status = null;
})
