// twopart-request.js 
var zmq = require( 'zmq' ) 
  , socket = zmq.socket( 'rep' )
  ;

// Each message part will come across as parameter 
socket.on('message', function( method, version, uri, accepts ){
	accepts = accepts.toString();

	if( accepts !== 'json'){
		return socket.send([400, 'BAD REQUEST', null ])
	} 
    socket.send([
    	200
    	, "OK"
    	, JSON.stringify({
    		data:[
				Math.random()
			  , Math.random()
			  , Math.random()
			  , Math.random()
			  , Math.random()
			  , Math.random()
			  , Math.random()
			  , Math.random()
			  , Math.random()
    		]
    	})
	])
});
// connect to original address 
socket.connect('tcp://0.0.0.0:9998');
