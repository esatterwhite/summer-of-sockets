// twopart-sender.js
var zmq = require( 'zmq' ) 
  , socket = zmq.socket( 'push' ) 

// bind to an address and port 
socket.bind('tcp://0.0.0.0:9998');
// send a message to forwarder
setInterval(function(){
    socket.send("multi", zmq.ZMQ_SNDMORE ); 
    socket.send( 2 ); 
},150); 
