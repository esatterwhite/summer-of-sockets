var zmq = require('zmq')
   ,server = zmq.socket('rep')
   ,count = 0;
// listen for the message event.
server.on('message', function(d){
	// make every 3rd message wait for a bit...
	setTimeout(function(){
		server.send(JSON.stringify({response:d.toString()}))
	}, ++count % 3 === 0 ? 1000 : 1 )
})
// bind to port 5454
server.bind('tcp://0.0.0.0:5454', function(err){
	if(err){
		console.error("something bad happened")
		console.error( err.msg )
		console.error( err.stack )
		process.exit(0)
	}
})


