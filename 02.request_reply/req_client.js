var zmq = require('zmq')
   ,client = zmq.socket('req')
   ,msgcount = 0;
// listen for responses from the server
client.on('message', function(d){
	console.log(JSON.parse( d ))
})
// connect to the server port
client.connect('tcp://0.0.0.0:5454');
setInterval( function(){
	client.send(msgcount++)
	// spit out the number of queued requests
	console.log( "%d message queued", client._outgoing.length )
},100)
