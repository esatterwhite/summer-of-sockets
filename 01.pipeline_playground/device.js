var zmq = require( 'zmq' ) 
  , backend = zmq.socket( 'pull' )
  , frontend = zmq.socket( 'push' ); 

// bind to an address and port 
backend.bind('tcp://0.0.0.0:9998', function( err ){ 
    if( err ){ 
        console.log("Backend socket %s ", err.message); 
        process.exit(0); 
    } 
    backend.on( 'message', function( msg ){
		
        frontend.send( msg )
	}); 
});

frontend.bind('tcp://0.0.0.0:9999', function( err ){
    if( err ){
        console.log("Front end socket %s", err.message);
        process.exit();
    }	
});
