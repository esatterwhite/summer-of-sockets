// multi_consumer.js
var zmq = require( 'zmq' ) 
  , socket = zmq.socket( 'pull' ) 

socket.on('message', function( msg ){
    console.log("got forwarded message %s", msg.toString());
});
// bind to an address and port 
socket.connect('tcp://0.0.0.0:9999');

