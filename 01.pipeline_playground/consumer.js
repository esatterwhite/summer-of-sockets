// consumer.js 
var zmq = require( 'zmq' ) 
  , socket = zmq.socket( 'pull' );
// messages come via events 
socket.on('message', function( msg ){ 
    // messages come as buffers 
    console.log("got message %s ", msg.toString('utf8')); 
});
// connect to original address 
socket.connect('tcp://0.0.0.0:9998');
